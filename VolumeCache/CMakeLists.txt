set(SRC VolumeCache.cpp)

set(VERSION 0.0.0)

add_library(VolumeCache SHARED ${SRC})

target_link_libraries(VolumeCache openvdb tbb boost_thread Half field3d hdf5)

install(TARGETS VolumeCache DESTINATION /Users/anders/vfx/volumecache/${VERSION}/lib)