#include <ai.h>

#include "VolumeCache/VolumeCache.h"
#include "common/remap.h"
#include "common/Color.h"

AI_SHADER_NODE_EXPORT_METHODS(alVolumeMtd)

#define NUM_BB_STEPS 10000
#define MAXK 10000
#define MINK 300

struct ShaderData
{    
    CacheInstance::Ptr densityCache;
    CacheInstance::Ptr temperatureCache;
    
    AtRGB scattering;
    float g;
    AtRGB absorption;

    bool densityRemap;
    float densityInputMin;
    float densityInputMax;
    bool densityClipMin;
    bool densityClipMax;
    float densityBias;
    float densityGain;
    float densityOutputMin;
    float densityOutputMax;

    bool temperatureRemap;
    float temperatureInputMin;
    float temperatureInputMax;
    bool temperatureClipMin;
    bool temperatureClipMax;
    float temperatureBias;
    float temperatureGain;
    float temperatureOutputMin;
    float temperatureOutputMax;

    AtRGB blackbody[NUM_BB_STEPS];
};

enum alVolumeParams
{
    p_filename,
    p_densityLayer,
    p_temperatureLayer,
    p_densityRemap,
    p_densityInputMin,
    p_densityInputMax,
    p_densityClipMin,
    p_densityClipMax,
    p_densityBias,
    p_densityGain,
    p_densityOutputMin,
    p_densityOutputMax,
    p_temperatureRemap,
    p_temperatureInputMin,
    p_temperatureInputMax,
    p_temperatureClipMin,
    p_temperatureClipMax,
    p_temperatureBias,
    p_temperatureGain,
    p_temperatureOutputMin,
    p_temperatureOutputMax,
    p_scattering,
    p_absorption,
    p_g,
    p_emissionStrength,
    p_emissionPhysicalIntensity,
};

node_parameters
{
    AiParameterStr("filename", "");
    AiParameterStr("densityLayer", "");
    AiParameterStr("temperatureLayer", "");
    AiParameterBool("densityRemap", false);
    AiParameterFlt("densityInputMin", 0.0f);
    AiParameterFlt("densityInputMax", 1.0f);
    AiParameterBool("densityClipMin", false);
    AiParameterBool("densityClipMax", false);
    AiParameterFlt("densityBias", 0.5f);
    AiParameterFlt("densityGain", 0.5f);
    AiParameterFlt("densityOutputMin", 0.0f);
    AiParameterFlt("densityOutputMax", 1.0f);
    AiParameterBool("temperatureRemap", false);
    AiParameterFlt("temperatureInputMin", 0.0f);
    AiParameterFlt("temperatureInputMax", 1.0f);
    AiParameterBool("temperatureClipMin", false);
    AiParameterBool("temperatureClipMax", false);
    AiParameterFlt("temperatureBias", 0.5f);
    AiParameterFlt("temperatureGain", 0.5f);
    AiParameterFlt("temperatureOutputMin", 0.0f);
    AiParameterFlt("temperatureOutputMax", 1.0f);
    AiParameterRGB("scattering", 0.5f, 0.5f, 0.5f);
    AiParameterRGB("absorption", 0.5f, 0.5f, 0.5f);
    AiParameterFlt("g", 0.0f);
    AiParameterFlt("emissionStrength", 0.0f);
    AiParameterFlt("emissionPhysicalIntensity", 1.0f);
}

node_loader
{
   if (i>0) return 0;
   node->methods     = alVolumeMtd;
   node->output_type = AI_TYPE_RGB;
   node->name        = "alVolume";
   node->node_type   = AI_NODE_SHADER;
   strcpy(node->version, AI_VERSION);
   return TRUE;
}

node_initialize
{
    ShaderData *data = new ShaderData;
    AiNodeSetLocalData(node,data);
}

node_finish
{
    if (AiNodeGetLocalData(node))
    {
        ShaderData* data = (ShaderData*) AiNodeGetLocalData(node);
        delete data;
        AiNodeSetLocalData(node, NULL);
    }
}

node_update
{
    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);

    const char* filename = params[p_filename].STR;
    const char* densityLayer = params[p_densityLayer].STR;
    VolumeCache::Handle h = VolumeCache::instance()->readFloatField(filename, densityLayer);
    if (h < 0)
    {
        AiMsgError("[alVolume] Could not load field '%s' from file '%s'", densityLayer, filename);
    }
    else
    {
        data->densityCache = VolumeCache::instance()->getCacheInstance(h);
    }

    
    std::string temperatureLayer = params[p_temperatureLayer].STR;
    if (temperatureLayer != "")
    {
        VolumeCache::Handle h = VolumeCache::instance()->readFloatField(filename, temperatureLayer);
        if (h < 0)
        {
            AiMsgError("[alVolume] Could not load field '%s' from file '%s'", temperatureLayer.c_str(), filename);
        }
        else
        {
            data->temperatureCache = VolumeCache::instance()->getCacheInstance(h);
        }
    }
    
    data->scattering = params[p_scattering].RGB;
    data->absorption = params[p_absorption].RGB;
    data->densityInputMin = params[p_densityInputMin].FLT;

    data->densityRemap = params[p_densityRemap].BOOL;
    data->densityInputMin = params[p_densityInputMin].FLT;
    data->densityInputMax = params[p_densityInputMax].FLT;
    data->densityClipMin = params[p_densityClipMin].BOOL;
    data->densityClipMax = params[p_densityClipMax].BOOL;
    data->densityBias = params[p_densityBias].FLT;
    data->densityGain = params[p_densityGain].FLT;
    data->densityOutputMin = params[p_densityOutputMin].FLT;
    data->densityOutputMax = params[p_densityOutputMax].FLT;

    data->temperatureRemap = params[p_temperatureRemap].BOOL;
    data->temperatureInputMin = params[p_temperatureInputMin].FLT;
    data->temperatureInputMax = params[p_temperatureInputMax].FLT;
    data->temperatureClipMin = params[p_temperatureClipMin].BOOL;
    data->temperatureClipMax = params[p_temperatureClipMax].BOOL;
    data->temperatureBias = params[p_temperatureBias].FLT;
    data->temperatureGain = params[p_temperatureGain].FLT;
    data->temperatureOutputMin = params[p_temperatureOutputMin].FLT;
    data->temperatureOutputMax = params[p_temperatureOutputMax].FLT;

    float pe = params[p_emissionPhysicalIntensity].FLT;

    float kstep = 1.0f;
    int i=0;
    BlackbodySpectrum bs;
    for (float k = 0; k < NUM_BB_STEPS; k += kstep)
    {
        bs.temp = k;
        AtRGB xyz = spectrumToXyz(bs);
        data->blackbody[i++] = xyzToRgb(CsRec709, xyz) * lerp(1.0f, powf(k, 4) * 5.67e-8 * powf(2.0f, -16.0f), pe);
    }
}

shader_evaluate
{
    // Scattering and absorption parameters
    AtRGB scattering = AiShaderEvalParamRGB(p_scattering);
    AtRGB absorption = AiShaderEvalParamRGB(p_absorption);
    AtFloat g = AiShaderEvalParamFlt(p_g);
    AtRGB emission = AI_RGB_BLACK;
    float emissionStrength = AiShaderEvalParamFlt(p_emissionStrength);

    ShaderData *data = (ShaderData*)AiNodeGetLocalData(node);

    float densityOutputMax = AiShaderEvalParamFlt(p_densityOutputMax);
    
    
    // Our lookup point
    Imath::V3d wsP(sg->Po.x, sg->Po.y, sg->Po.z);
    
    // Read density, remap and apply to the scattering parameters
    float d = 0.0f;
    if (data && data->densityCache)
    {
       
        d = data->densityCache->lookup(wsP);
    }

    if (data->densityRemap)
    {
        d = remap(d, data->densityInputMin, data->densityInputMax, data->densityClipMin, data->densityClipMax, data->densityBias, data->densityGain,
                data->densityOutputMin, densityOutputMax);
        // just to make sure...
        d = std::max(0.0f, d);
    }

    scattering *= d;
    absorption *= d;
    
    AiShaderGlobalsSetVolumeScattering(sg, scattering, g);
    AiShaderGlobalsSetVolumeAbsorption(sg, absorption);

    // Read temperature, remap and if significant, calculate blackbody emission
    if (emissionStrength > 0.0f)
    {
        float temperatureOutputMax = AiShaderEvalParamFlt(p_temperatureOutputMax);
        float t = 0.0f;
        if (data && data->temperatureCache)
        {
            t = data->temperatureCache->lookup(wsP);
        }

        if (data->temperatureRemap)
        {
            t = remap(t, data->temperatureInputMin, data->temperatureInputMax, data->temperatureClipMin, data->temperatureClipMax, data->temperatureBias, data->temperatureGain,
                    data->temperatureOutputMin, temperatureOutputMax);
        }
        
        if (t > 275)
        {
            int i = std::min(int(t), NUM_BB_STEPS-1);
            emission = data->blackbody[i] * emissionStrength;
        }

        emission *= d;
        AiShaderGlobalsSetVolumeEmission(sg, emission);
    }
}