//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

#include "alVolumeNode.h"
#include "VolumeCache/VolumeCache.h"
#include "common/remap.h"
#include <maya/MFnStringData.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MAnimControl.h>
#include <sstream>

enum {
        kDrawWireframe,
        kDrawWireframeOnShaded,
        kDrawSmoothShaded,
        kDrawFlatShaded,
        kLastToken
    };
#define LEAD_COLOR              18  // green
#define ACTIVE_COLOR            15  // white
#define ACTIVE_AFFECTED_COLOR   8   // purple
#define DORMANT_COLOR           4   // blue
#define HILITE_COLOR            17  // pale blue

#define RETURN_ON_ERROR(stat, msg) \
if (!stat) \
{ \
    stat.perror((MString(__PRETTY_FUNCTION__)+MString(" | ") + MString(msg))); \
    return stat; \
}

#define PRINT_ON_ERROR(stat, msg) \
if (!stat) \
{ \
    stat.perror((MString(__PRETTY_FUNCTION__)+MString(" | ") + MString(msg))); \
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Node implementation with standard viewport draw
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MObject alVolumeNode::_aVolumePath;
MObject alVolumeNode::_aResolvedVolumePath;
MObject alVolumeNode::_aDensityLayer;
MObject alVolumeNode::_aTemperatureLayer;
MObject alVolumeNode::_aDummy;

MObject alVolumeNode::_aDensityRemap;
MObject alVolumeNode::_aDensityInputMin;
MObject alVolumeNode::_aDensityInputMax;
MObject alVolumeNode::_aDensityClipMin;
MObject alVolumeNode::_aDensityClipMax;
MObject alVolumeNode::_aDensityBias;
MObject alVolumeNode::_aDensityGain;
MObject alVolumeNode::_aDensityOutputMin;
MObject alVolumeNode::_aDensityOutputMax;

MObject alVolumeNode::_aTemperatureRemap;
MObject alVolumeNode::_aTemperatureInputMin;
MObject alVolumeNode::_aTemperatureInputMax;
MObject alVolumeNode::_aTemperatureClipMin;
MObject alVolumeNode::_aTemperatureClipMax;
MObject alVolumeNode::_aTemperatureBias;
MObject alVolumeNode::_aTemperatureGain;
MObject alVolumeNode::_aTemperatureOutputMin;
MObject alVolumeNode::_aTemperatureOutputMax;

MObject alVolumeNode::_aStepSize;
MObject alVolumeNode::_aScattering;
MObject alVolumeNode::_aAbsorption;
MObject alVolumeNode::_aG;

MObject alVolumeNode::_aEmissionStrength;
MObject alVolumeNode::_aPhysicalIntensity;

MObject alVolumeNode::_aPreviewStyle;
MObject alVolumeNode::_aTime;

MTypeId alVolumeNode::id( 0x80010 );

alVolumeNode::alVolumeNode() : _valid(false) {}
alVolumeNode::~alVolumeNode() {}

class StringExpander
{
public:
    StringExpander(double time)
    : _time(time)
    {
        _frame = int(floor(time));
    }

    std::string expand(const std::string& str)
    {
        std::string result = str;
        size_t pos = result.find("$F4");
        if (pos != std::string::npos)
        {
            std::stringstream ss;
            ss << std::setw(4) << std::setfill('0') << _frame;
            result.erase(pos, 3);
            result.insert(pos, ss.str());

            expand(result);
        }

        return result;
    }


private:
    double _time;
    int _frame;
};

int getCurrentFrame(const MTime& t)
{
    return int(t.as(MTime::uiUnit()));
}

MStatus alVolumeNode::compute( const MPlug& plug, MDataBlock& data )
{
    MStatus stat;

    if (plug == _aDummy)
    {
        MDataHandle hDummy = data.outputValue(_aDummy, &stat);
        RETURN_ON_ERROR(stat, "data.outputValue(_aDummy)");
        data.setClean(plug);

        return MS::kSuccess;
    }
    else if (plug == _aResolvedVolumePath)
    {
        MDataHandle hVolumePath = data.inputValue(_aVolumePath, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aVolumePath)");
        std::string volumePath = hVolumePath.asString().asChar();

        MDataHandle hDensityLayer = data.inputValue(_aDensityLayer, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityLayer)");
        std::string densityLayer = hDensityLayer.asString().asChar();

        MDataHandle hDensityRemap = data.inputValue(_aDensityRemap, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityRemap)");
        bool densityRemap = hDensityRemap.asBool();

        MDataHandle hDensityInputMin = data.inputValue(_aDensityInputMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityInputMin)");
        float densityInputMin = hDensityInputMin.asFloat();

        MDataHandle hDensityInputMax = data.inputValue(_aDensityInputMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityInputMax)");
        float densityInputMax = hDensityInputMax.asFloat();

        MDataHandle hDensityClipMin = data.inputValue(_aDensityClipMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityClipMin)");
        bool densityClipMin = hDensityClipMin.asBool();

        MDataHandle hDensityClipMax = data.inputValue(_aDensityClipMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityClipMax)");
        bool densityClipMax = hDensityClipMax.asBool();

        MDataHandle hDensityBias = data.inputValue(_aDensityBias, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityBias)");
        float densityBias = hDensityBias.asFloat();

        MDataHandle hDensityGain = data.inputValue(_aDensityGain, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityGain)");
        float densityGain = hDensityGain.asFloat();

        MDataHandle hDensityOutputMin = data.inputValue(_aDensityOutputMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityOutputMin)");
        float densityOutputMin = hDensityOutputMin.asFloat();

        MDataHandle hDensityOutputMax = data.inputValue(_aDensityOutputMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aDensityOutputMax)");
        float densityOutputMax = hDensityOutputMax.asFloat();

        MDataHandle hTemperatureLayer = data.inputValue(_aTemperatureLayer, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureLayer)");
        std::string temperatureLayer = hTemperatureLayer.asString().asChar();

        MDataHandle hTemperatureRemap = data.inputValue(_aTemperatureRemap, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureRemap)");
        bool temperatureRemap = hTemperatureRemap.asBool();

        MDataHandle hTemperatureInputMin = data.inputValue(_aTemperatureInputMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureInputMin)");
        float temperatureInputMin = hTemperatureInputMin.asFloat();

        MDataHandle hTemperatureInputMax = data.inputValue(_aTemperatureInputMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureInputMax)");
        float temperatureInputMax = hTemperatureInputMax.asFloat();

        MDataHandle hTemperatureClipMin = data.inputValue(_aTemperatureClipMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureClipMin)");
        bool temperatureClipMin = hTemperatureClipMin.asBool();

        MDataHandle hTemperatureClipMax = data.inputValue(_aTemperatureClipMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureClipMax)");
        bool temperatureClipMax = hTemperatureClipMax.asBool();

        MDataHandle hTemperatureBias = data.inputValue(_aTemperatureBias, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureBias)");
        float temperatureBias = hTemperatureBias.asFloat();

        MDataHandle hTemperatureGain = data.inputValue(_aTemperatureGain, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureGain)");
        float temperatureGain = hTemperatureGain.asFloat();

        MDataHandle hTemperatureOutputMin = data.inputValue(_aTemperatureOutputMin, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureOutputMin)");
        float temperatureOutputMin = hTemperatureOutputMin.asFloat();

        MDataHandle hTemperatureOutputMax = data.inputValue(_aTemperatureOutputMax, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aTemperatureOutputMax)");
        float temperatureOutputMax = hTemperatureOutputMax.asFloat();

        MDataHandle hPreviewStyle = data.inputValue(_aPreviewStyle, &stat);
        RETURN_ON_ERROR(stat, "data.inputValue(_aPreviewStyle)");
        int previewStyle = hPreviewStyle.asInt();

        MTime t = data.inputValue(_aTime).asTime();

        _valid= false;

        if (volumePath == "")
            return MS::kSuccess;

        StringExpander se(getCurrentFrame(t));
        volumePath = se.expand(volumePath);

        VolumeCache::Handle cacheHandle  = VolumeCache::instance()->readFloatField(volumePath, densityLayer);
        if (cacheHandle >= 0)
        {
            CacheInstance::Ptr c = VolumeCache::instance()->getCacheInstance(cacheHandle);
            if (c)
            {
                if (c->cacheType() == CacheInstance::kField3D)
                {
                    Field3DCacheInstance::Ptr f3dc = boost::dynamic_pointer_cast<Field3DCacheInstance>(c);
                
                    // Transform the extents to world space
                    Imath::V3f minw, maxw;
                    f3dc->_mapping->localToWorld().multVecMatrix(Imath::V3f(0,0,0), minw);
                    f3dc->_mapping->localToWorld().multVecMatrix(Imath::V3f(1,1,1), maxw);

                    _bbmin = MPoint(minw.x, minw.y, minw.z);
                    _bbmax = MPoint(maxw.x, maxw.y, maxw.z);

                    if (MGlobal::mayaState() == MGlobal::kInteractive && previewStyle > kBoundingBox)
                    {
                        
                        if (_geometry.displayList) glDeleteLists(_geometry.displayList,1);
                        _geometry.displayList = glGenLists(1);
                        glNewList(_geometry.displayList,GL_COMPILE);

                        glPointSize(2);
                        glBegin(GL_POINTS);

                        Imath::V3i minv = f3dc->_field->dataWindow().min;
                        Imath::V3i maxv = f3dc->_field->dataWindow().max;
                        
                        for (int z = minv.z; z < maxv.z; ++z)
                        {
                            for (int y = minv.y; y < maxv.y; ++y)
                            {
                                for (int x = minv.x; x < maxv.x; ++x)
                                {
                                    Imath::V3d p;
                                    float d = f3dc->_field->fastValue(x,y,z);
                                    if (densityRemap)
                                    {
                                        d = remap(d, densityInputMin, densityInputMax, densityClipMin, densityClipMax, 
                                                    densityBias, densityGain, densityOutputMin, densityOutputMax);
                                    }
                                    if (d > 0.0f)
                                    {
                                        glColor3f(d,d,d);
                                        f3dc->_mapping->voxelToWorld().multVecMatrix(Imath::V3d(double(x)+0.5,double(y)+0.5,double(z)+0.5), p);
                                        glVertex3d(p.x, p.y, p.z);
                                    }
                                }
                            }
                        }

                        glEnd();
                        glEndList();
                        
                        
                    }

                    
                }
                else if (c->cacheType() == CacheInstance::kOpenVDB)
                {
                    OpenVDBCacheInstance::Ptr oc = boost::dynamic_pointer_cast<OpenVDBCacheInstance>(c);

                    if (_geometry.displayList) glDeleteLists(_geometry.displayList,1);
                    _geometry.displayList = glGenLists(1);
                    glNewList(_geometry.displayList,GL_COMPILE);

                    glPointSize(2);
                    glBegin(GL_POINTS);
                    Imath::V3d bbmin, bbmax;
                    oc->bounds(bbmin, bbmax);
                    _bbmin = MPoint(bbmin.x, bbmin.y, bbmin.z);
                    _bbmax = MPoint(bbmax.x, bbmax.y, bbmax.z);
                    for (FloatGridType::ValueOnCIter it = oc->_grid->cbeginValueOn(); it.test(); ++it)
                    {
                        if (it.isVoxelValue())
                        {
                            openvdb::Vec3d P = it.getCoord().asVec3d();
                            P = oc->_grid->indexToWorld(P);
                            float d = *it;
                            if (d > 0.0f)
                            {
                                glColor3f(d,d,d);
                                glVertex3d(P.x(), P.y(), P.z());
                            }
                        }
                    }

                    glEnd();
                    glEndList();
                    
                }
                else
                {
                    MGlobal::displayError(MString("[VolumeCache] Unkown cache type"));
                    _resolvedVolumePath = "";
                    return MS::kFailure;
                }

                _resolvedVolumePath = volumePath;
                MGlobal::displayInfo(MString("[VolumeCache] loaded ") + MString(volumePath.c_str())+ MString(" ok"));
                Imath::V3d voxelSize = c->voxelSize();
                std::stringstream ss;
                ss << "[VolumeCache] voxel size is " << voxelSize;
                MGlobal::displayInfo(MString(ss.str().c_str()));
                
                _voxelSize.x = voxelSize.x;
                _voxelSize.y = voxelSize.y;
                _voxelSize.z = voxelSize.z;

                _valid = true;
                
                float mem = VolumeCache::instance()->memUsage();
                mem /= 1000000.0f;
                std::cout << "[VolumeCache] mem usage: " << std::setprecision(2) << mem << "MB" << std::endl;
                
                MDataHandle hResolvedVolumePath = data.outputValue(_aResolvedVolumePath, &stat);
                hResolvedVolumePath.set(MString(_resolvedVolumePath.c_str()));
                RETURN_ON_ERROR(stat, "data.outputValue(_aDummy)");

                return MS::kSuccess;

                
            }
            else
            {
                MGlobal::displayError(MString("[VolumeCache] Returned NULL CacheInstance"));
                _resolvedVolumePath = "";
                return MS::kFailure;
            }

            
        }
        else
        {
            MGlobal::displayError(MString("[VolumeCache] could not load cache ") + MString(_resolvedVolumePath.c_str()));
            _resolvedVolumePath = "";
            return MS::kFailure;
        }
        
    }
    else return MS::kUnknownParameter;
}



void* alVolumeNode::getGeometry()
{
    // Force a compute if needed
    //
    MObject thisNode = thisMObject();
    MStatus stat;
    MPlug pResolvedVolumePath(thisNode, _aResolvedVolumePath);
    MString path = pResolvedVolumePath.asString();

    MPlug pPreviewStyle(thisNode, _aPreviewStyle);
    int previewStyle = pPreviewStyle.asInt();

    void* ret = NULL;
    
    if (_valid)
    {
        _geometry.mn = _bbmin;
        _geometry.mx = _bbmax;
        _geometry.previewStyle = previewStyle;
        ret = &_geometry;
    }

    return ret;
    
}

bool alVolumeNode::isBounded() const
{
    return true;
}

MBoundingBox alVolumeNode::boundingBox() const
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MObject thisNode = thisMObject();
    MStatus stat;
    MPlug pResolvedVolumePath(thisNode, _aResolvedVolumePath);
    MString path = pResolvedVolumePath.asString();
    MBoundingBox bb;
    bb.expand(_bbmin);
    bb.expand(_bbmax);
    //std::cerr << _bbmin << " " << _bbmax << std::endl;
    return bb;
}

void* alVolumeNode::creator()
{
    return new alVolumeNode();
}


//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Plugin Registration
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MStatus alVolumeNode::initialize()
{
    MFnTypedAttribute tAttr;
    MFnNumericAttribute nAttr;
    MFnEnumAttribute eAttr;
    MFnUnitAttribute uAttr;
    MFnStringData stringData;
    MStatus stat;
    _aVolumePath = tAttr.create("volumePath", "vp", MFnData::kString, &stat);
    tAttr.setStorable(true);
    tAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create volumePath");

    _aResolvedVolumePath = tAttr.create("resolvedVolumePath", "rvp", MFnData::kString, &stat);
    tAttr.setStorable(false);
    tAttr.setWritable(false);
    tAttr.setReadable(true);
    RETURN_ON_ERROR(stat, "create resolvedVolumePath");

    _aDensityLayer = tAttr.create("densityLayer", "dl", MFnData::kString, &stat);
    tAttr.setDefault(stringData.create("density"));
    tAttr.setStorable(true);
    tAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityLayer");

    _aTemperatureLayer = tAttr.create("temperatureLayer", "tl", MFnData::kString, &stat);
    tAttr.setDefault(stringData.create("heat"));
    tAttr.setStorable(true);
    tAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureLayer");

    _aDummy = nAttr.create("dummy", "d", MFnNumericData::kInt, 0, &stat);
    nAttr.setStorable(false);
    nAttr.setWritable(false);
    nAttr.setReadable(true);
    RETURN_ON_ERROR(stat, "create dummy");

    _aDensityRemap = nAttr.create("densityRemap", "dr", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityRemap");

    _aDensityInputMin = nAttr.create("densityInputMin", "din", MFnNumericData::kFloat, 0.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityInputMin");

    _aDensityInputMax = nAttr.create("densityInputMax", "dix", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityInputMax");

    _aDensityClipMin = nAttr.create("densityClipMin", "dcn", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityClipMin");

    _aDensityClipMax = nAttr.create("densityClipMax", "dcx", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityClipMax");

    _aDensityBias = nAttr.create("densityBias", "db", MFnNumericData::kFloat, 0.5f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityBias");

    _aDensityGain = nAttr.create("densityGain", "dg", MFnNumericData::kFloat, 0.5f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityGain");

    _aDensityOutputMin = nAttr.create("densityOutputMin", "don", MFnNumericData::kFloat, 0.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityOutputMin");

    _aDensityOutputMax = nAttr.create("densityOutputMax", "dox", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create densityOutputMax");

    _aTemperatureRemap = nAttr.create("temperatureRemap", "tr", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureRemap");

    _aTemperatureInputMin = nAttr.create("temperatureInputMin", "tin", MFnNumericData::kFloat, 0.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureInputMin");

    _aTemperatureInputMax = nAttr.create("temperatureInputMax", "tix", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureInputMax");

    _aTemperatureClipMin = nAttr.create("temperatureClipMin", "tcn", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureClipMin");

    _aTemperatureClipMax = nAttr.create("temperatureClipMax", "tcx", MFnNumericData::kBoolean, false, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureClipMax");

    _aTemperatureBias = nAttr.create("temperatureBias", "tb", MFnNumericData::kFloat, 0.5f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureBias");

    _aTemperatureGain = nAttr.create("temperatureGain", "tg", MFnNumericData::kFloat, 0.5f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureGain");

    _aTemperatureOutputMin = nAttr.create("temperatureOutputMin", "ton", MFnNumericData::kFloat, 0.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureOutputMin");

    _aTemperatureOutputMax = nAttr.create("temperatureOutputMax", "tox", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create temperatureOutputMax");

    _aScattering = nAttr.createColor("scattering", "sc", &stat);
    nAttr.setDefault(0.5f, 0.5f, 0.5f);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    nAttr.setUsedAsColor(true);
    RETURN_ON_ERROR(stat, "create scattering");

    _aAbsorption = nAttr.createColor("absorption", "ab", &stat);
    nAttr.setDefault(0.5f, 0.5f, 0.5f);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    nAttr.setUsedAsColor(true);
    RETURN_ON_ERROR(stat, "create absorption");

    _aStepSize = nAttr.create("stepSize", "ss", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create stepSize");

    _aG = nAttr.create("g", "g", MFnNumericData::kFloat, 0.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create g");

    _aEmissionStrength = nAttr.create("emissionStrength", "es", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create emissionStrength");

    _aPhysicalIntensity = nAttr.create("physicalIntensity", "pi", MFnNumericData::kFloat, 1.0f, &stat);
    nAttr.setStorable(true);
    nAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create physicalIntensity");

    _aPreviewStyle = eAttr.create("previewStyle", "ps", kBoundingBox, &stat);
    eAttr.setStorable(true);
    eAttr.setWritable(true);
    eAttr.addField("Bounding box", kBoundingBox);
    eAttr.addField("Density points", kDensityPoints);
    eAttr.addField("Emission points", kEmissionPoints);
    RETURN_ON_ERROR(stat, "create previewStyle");

    _aTime = uAttr.create("time", "tm", MFnUnitAttribute::kTime, 0, &stat);
    uAttr.setStorable(true);
    uAttr.setWritable(true);
    RETURN_ON_ERROR(stat, "create time");


    addAttribute(_aVolumePath);
    addAttribute(_aResolvedVolumePath);
    addAttribute(_aDensityLayer);
    addAttribute(_aTemperatureLayer);
    addAttribute(_aDensityRemap);
    addAttribute(_aDensityInputMin);
    addAttribute(_aDensityInputMax);
    addAttribute(_aDensityClipMin);
    addAttribute(_aDensityClipMax);
    addAttribute(_aDensityBias);
    addAttribute(_aDensityGain);
    addAttribute(_aDensityOutputMin);
    addAttribute(_aDensityOutputMax);
    addAttribute(_aDummy);
    addAttribute(_aTemperatureRemap);
    addAttribute(_aTemperatureInputMin);
    addAttribute(_aTemperatureInputMax);
    addAttribute(_aTemperatureClipMin);
    addAttribute(_aTemperatureClipMax);
    addAttribute(_aTemperatureBias);
    addAttribute(_aTemperatureGain);
    addAttribute(_aTemperatureOutputMin);
    addAttribute(_aTemperatureOutputMax);
    addAttribute(_aScattering);
    addAttribute(_aG);
    addAttribute(_aAbsorption);
    addAttribute(_aStepSize);
    addAttribute(_aEmissionStrength);
    addAttribute(_aPhysicalIntensity);
    addAttribute(_aPreviewStyle);
    addAttribute(_aTime);


    attributeAffects(_aVolumePath, _aResolvedVolumePath);
    attributeAffects(_aDensityLayer, _aResolvedVolumePath);
    attributeAffects(_aDensityRemap, _aResolvedVolumePath);
    attributeAffects(_aDensityInputMin, _aResolvedVolumePath);
    attributeAffects(_aDensityInputMax, _aResolvedVolumePath);
    attributeAffects(_aDensityClipMin, _aResolvedVolumePath);
    attributeAffects(_aDensityClipMax, _aResolvedVolumePath);
    attributeAffects(_aDensityBias, _aResolvedVolumePath);
    attributeAffects(_aDensityGain, _aResolvedVolumePath);
    attributeAffects(_aDensityOutputMin, _aResolvedVolumePath);
    attributeAffects(_aDensityOutputMax, _aResolvedVolumePath);
    attributeAffects(_aTemperatureRemap, _aResolvedVolumePath);
    attributeAffects(_aTemperatureInputMin, _aResolvedVolumePath);
    attributeAffects(_aTemperatureInputMax, _aResolvedVolumePath);
    attributeAffects(_aTemperatureClipMin, _aResolvedVolumePath);
    attributeAffects(_aTemperatureClipMax, _aResolvedVolumePath);
    attributeAffects(_aTemperatureBias, _aResolvedVolumePath);
    attributeAffects(_aTemperatureGain, _aResolvedVolumePath);
    attributeAffects(_aTemperatureOutputMin, _aResolvedVolumePath);
    attributeAffects(_aTemperatureOutputMax, _aResolvedVolumePath);
    attributeAffects(_aPreviewStyle, _aResolvedVolumePath);
    attributeAffects(_aTime, _aResolvedVolumePath);

    return MS::kSuccess;
}

void drawBox(Geometry* g)
{
    glBegin( GL_LINES );

    for ( int axis=0; axis<3; ++axis )
    {
    int axis0 = ( axis + 1 ) % 3;
    int axis1 = ( axis + 2 ) % 3;

    MPoint mn, mx;
    mn[axis]=g->mn[axis];
    mx[axis]=g->mx[axis];

    // min, min
    mn[axis0]=g->mn[axis0];
    mx[axis0]=g->mn[axis0];
    mn[axis1]=g->mn[axis1];
    mx[axis1]=g->mn[axis1];
    glVertex3f(mn.x,mn.y,mn.z);
    glVertex3f(mx.x,mx.y,mx.z);

    // min, max
    mn[axis0]=g->mn[axis0];
    mx[axis0]=g->mn[axis0];
    mn[axis1]=g->mx[axis1];
    mx[axis1]=g->mx[axis1];
    glVertex3f(mn.x,mn.y,mn.z);
    glVertex3f(mx.x,mx.y,mx.z);

    // max, min
    mn[axis0]=g->mx[axis0];
    mx[axis0]=g->mx[axis0];
    mn[axis1]=g->mn[axis1];
    mx[axis1]=g->mn[axis1];
    glVertex3f(mn.x,mn.y,mn.z);
    glVertex3f(mx.x,mx.y,mx.z);

    // max, max
    mn[axis0]=g->mx[axis0];
    mx[axis0]=g->mx[axis0];
    mn[axis1]=g->mx[axis1];
    mx[axis1]=g->mx[axis1];
    glVertex3f(mn.x,mn.y,mn.z);
    glVertex3f(mx.x,mx.y,mx.z);
    }       

    glEnd();    
}


void alVolumeNodeUI::draw(const MDrawRequest& request, M3dView& view) const
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MDrawData data = request.drawData();

    Geometry* geo = (Geometry*)data.geometry();
    if (!geo) return;

    view.beginGL();

        drawBox(geo);

        if (geo->previewStyle > alVolumeNode::kBoundingBox)
        {
            glCallList(geo->displayList);
        }

    view.endGL();
}

void alVolumeNodeUI::getDrawRequests(const MDrawInfo& info, bool objectAndActiveOnly, MDrawRequestQueue& queue)
{
   //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MDrawData data;
    MDrawRequest request = info.getPrototype(*this);
    alVolumeNode* volumeNode = (alVolumeNode*)surfaceShape();
    getDrawData(volumeNode->getGeometry(), data);
    request.setDrawData(data);

    switch (info.displayStyle())
    {
    case M3dView::kWireFrame:
        request.setToken(kDrawWireframe);
        break;
    case M3dView::kGouraudShaded :
        request.setToken(kDrawSmoothShaded);
        break;
    case M3dView::kFlatShaded :
        request.setToken(kDrawFlatShaded);
        break;
    default:
        break;
    }

    M3dView::DisplayStatus displayStatus = info.displayStatus();
    M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
    M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
    switch ( displayStatus )
    {
        case M3dView::kLead :
            request.setColor( LEAD_COLOR, activeColorTable );
            break;
        case M3dView::kActive :
            request.setColor( ACTIVE_COLOR, activeColorTable );
            break;
        case M3dView::kActiveAffected :
            request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
            break;
        case M3dView::kDormant :
            request.setColor( DORMANT_COLOR, dormantColorTable );
            break;
        case M3dView::kHilite :
            request.setColor( HILITE_COLOR, activeColorTable );
            break;
        default:    
            break;
    }

    queue.add(request);
}

bool alVolumeNodeUI::select( MSelectInfo &selectInfo,
                             MSelectionList &selectionList,
                             MPointArray &worldSpaceSelectPts ) const
//
// Select function. Gets called when the bbox for the object is selected.
// This function just selects the object without doing any intersection tests.
//
{
    //std::cerr << __PRETTY_FUNCTION__ << std::endl;
    MSelectionMask priorityMask( MSelectionMask::kSelectObjectsMask );
    MSelectionList item;
    item.add( selectInfo.selectPath() );
    MPoint xformedPt;
    selectInfo.addSelection( item, xformedPt, selectionList,
                             worldSpaceSelectPts, priorityMask, false );
    return true;
}

void* alVolumeNodeUI::creator()
{
    return new alVolumeNodeUI;
}


void* alVolumeNodeTranslator::create()
{
    return new alVolumeNodeTranslator;
}

AtNode* alVolumeNodeTranslator::CreateArnoldNodes()
{
    AtNode* ret = NULL;
    // create a box for the volume container shape
    if (GetMayaDagPath().isVisible())
    {
        MString boxPath = GetMayaDagPath().fullPathName();
        MString volumePath = boxPath + MString("_shader");

        ret = AddArnoldNode("box", boxPath.asChar());
        AtNode* v = AddArnoldNode("alVolume", volumePath.asChar());
        AiNodeSetPtr(ret, "shader", v);
    }
    return ret;
}

void alVolumeNodeTranslator::Export(AtNode* node)
{
    Update(node);
}

void alVolumeNodeTranslator::Update(AtNode* node)
{
    MPlug plug;

    MFnDagNode fnDagNode(GetMayaDagPath());
    MFnDependencyNode fnDepNode(GetMayaDagPath().node());
    MPoint min = fnDagNode.boundingBox().min();
    MPoint max = fnDagNode.boundingBox().max();
    AiNodeSetPnt(node, "min", min.x, min.y, min.z);
    AiNodeSetPnt(node, "max", max.x, max.y, max.z);

    MMatrix xform = GetMayaDagPath().inclusiveMatrix();
    alVolumeNode* vn = dynamic_cast<alVolumeNode*>(fnDepNode.userNode());

    MVector vsx = vn->_voxelSize * xform;

    plug = FindMayaObjectPlug("stepSize");
    float detail = plug.asFloat();
    // interpret the step size attribute as a multiplier on the world-space voxel size of the cache
    float stepSize = std::max(vsx.x, std::max(vsx.y, vsx.z));
    stepSize /= detail;

    AiNodeSetFlt(node, "step_size", stepSize);

    ExportMatrix(node, 0);

    // export shader params
    AtNode* shader = (AtNode*)AiNodeGetPtr(node, "shader");
    plug = FindMayaObjectPlug("densityMult");
    AiNodeSetFlt(shader, "densityMult", plug.asFloat());

    plug = FindMayaObjectPlug("densityClip");
    AiNodeSetFlt(shader, "densityClip", plug.asFloat());

    float r, g, b;
    plug = FindMayaObjectPlug("scattering");
    r = plug.child(0).asFloat();
    g = plug.child(1).asFloat();
    b = plug.child(2).asFloat();
    AiNodeSetRGB(shader, "scattering", r, g, b);

    plug = FindMayaObjectPlug("g");
    AiNodeSetFlt(shader, "g", plug.asFloat());

    plug = FindMayaObjectPlug("absorption");
    r = plug.child(0).asFloat();
    g = plug.child(1).asFloat();
    b = plug.child(2).asFloat();
    AiNodeSetRGB(shader, "absorption", r, g, b);

    plug = FindMayaObjectPlug("resolvedVolumePath"); 
    MString path = plug.asString();
    AiNodeSetStr(shader, "filename", path.asChar());

    plug = FindMayaObjectPlug("densityLayer");
    AiNodeSetStr(shader, "densityLayer", plug.asString().asChar());

    plug = FindMayaObjectPlug("temperatureLayer");
    AiNodeSetStr(shader, "temperatureLayer", plug.asString().asChar());

    plug = FindMayaObjectPlug("densityRemap");
    AiNodeSetBool(shader, "densityRemap", plug.asBool());

    plug = FindMayaObjectPlug("densityInputMin");
    AiNodeSetFlt(shader, "densityInputMin", plug.asFloat());

    plug = FindMayaObjectPlug("densityInputMax");
    AiNodeSetFlt(shader, "densityInputMax", plug.asFloat());

    plug = FindMayaObjectPlug("densityClipMin");
    AiNodeSetBool(shader, "densityClipMin", plug.asBool());

    plug = FindMayaObjectPlug("densityClipMax");
    AiNodeSetBool(shader, "densityClipMax", plug.asBool());

    plug = FindMayaObjectPlug("densityBias");
    AiNodeSetFlt(shader, "densityBias", plug.asFloat());

    plug = FindMayaObjectPlug("densityGain");
    AiNodeSetFlt(shader, "densityGain", plug.asFloat());

    plug = FindMayaObjectPlug("densityOutputMin");
    AiNodeSetFlt(shader, "densityOutputMin", plug.asFloat());

    plug = FindMayaObjectPlug("densityOutputMax");
    AiNodeSetFlt(shader, "densityOutputMax", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureRemap");
    AiNodeSetBool(shader, "temperatureRemap", plug.asBool());

    plug = FindMayaObjectPlug("temperatureInputMin");
    AiNodeSetFlt(shader, "temperatureInputMin", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureInputMax");
    AiNodeSetFlt(shader, "temperatureInputMax", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureClipMin");
    AiNodeSetBool(shader, "temperatureClipMin", plug.asBool());

    plug = FindMayaObjectPlug("temperatureClipMax");
    AiNodeSetBool(shader, "temperatureClipMax", plug.asBool());

    plug = FindMayaObjectPlug("temperatureBias");
    AiNodeSetFlt(shader, "temperatureBias", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureGain");
    AiNodeSetFlt(shader, "temperatureGain", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureOutputMin");
    AiNodeSetFlt(shader, "temperatureOutputMin", plug.asFloat());

    plug = FindMayaObjectPlug("temperatureOutputMax");
    AiNodeSetFlt(shader, "temperatureOutputMax", plug.asFloat());

    plug = FindMayaObjectPlug("emissionStrength");
    AiNodeSetFlt(shader, "emissionStrength", plug.asFloat());

    plug = FindMayaObjectPlug("physicalIntensity");
    AiNodeSetFlt(shader, "emissionPhysicalIntensity", plug.asFloat());

    
}

extern "C"
{

    void initializeExtension(CExtension& ext)
    {
        //std::cerr << __PRETTY_FUNCTION__ << std::endl;

        MStatus stat;
        MObject obj = CExtensionsManager::GetMayaPlugin();
        MFnPlugin plugin(obj, "AL", "3.0", "Any");
        stat = plugin.registerShape(
                "alVolumeNode", 
                alVolumeNode::id, 
                alVolumeNode::creator, 
                alVolumeNode::initialize, 
                alVolumeNodeUI::creator
            );
        PRINT_ON_ERROR(stat, "register alVolumeNode");
        ext.RegisterTranslator("alVolumeNode", "alVolumeNodeTranslator", alVolumeNodeTranslator::create);

        stat = MGlobal::executeCommand("source alVolumeNode");
        PRINT_ON_ERROR(stat, "source alVolumeNode.mel");
    }

    void deinitializeExtension(CExtension& ext)
    {

    }


}
