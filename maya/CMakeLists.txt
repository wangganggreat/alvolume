set(BUNDLE alVolumeNode)
set(SRC alVolumeNode.cpp ../common/Color.cpp)

set(MTOA_INCLUDE ${MTOA_ROOT}/include)

set(MAYA_VERSION 2013)
set(MAYA_LOCATION /Applications/Autodesk/maya${MAYA_VERSION})
set(MAYA_INCLUDE ${MAYA_LOCATION}/devkit/include)
set(DYNLIB_LOCATION ${MAYA_LOCATION}/Maya.app/Contents/MacOS)
set(CFLAGS "-D_BOOL -DREQUIRE_IOSTREAM -D_DARWIN -arch x86_64")
set(LFLAGS "-fno-gnu-keywords -fpascal-strings  -arch x86_64 -headerpad_max_install_names -framework System -framework SystemConfiguration -framework CoreServices -framework Carbon -framework Cocoa -framework ApplicationServices -framework IOKit")
set(LFLAGS "${LFLAGS} -lOpenMaya -lOpenMayaRender -lOpenMayaUI -lOpenMayaAnim -lFoundation -framework AGL -framework OpenGL")
set(LFLAGS "${LFLAGS} -L${DYNLIB_LOCATION} -Wl,-executable_path,${DYNLIB_LOCATION}")

set(CMAKE_CXX_FLAGS ${CFLAGS})
set(CMAKE_SHARED_LINKER_FLAGS ${LFLAGS})


include_directories(${MAYA_INCLUDE})
include_directories(${MTOA_INCLUDE})
include_directories("..")



link_directories(${MTOA_ROOT}/bin)


add_library(${BUNDLE} SHARED ${SRC})
target_link_libraries(${BUNDLE} ai mtoa_api VolumeCache field3d hdf5 boost_regex)
set_target_properties(${BUNDLE} PROPERTIES PREFIX "")
#set_target_properties(${BUNDLE} PROPERTIES SUFFIX ".bundle")
install(TARGETS ${BUNDLE} DESTINATION ${MTOA_ROOT}/extensions)
install(FILES AEalVolumeNodeTemplate.mel alVolumeNode.mel shelf_alVolumeNode.mel DESTINATION ${MTOA_ROOT}/scripts)
install(FILES alVolumeNode.png DESTINATION ${MTOA_ROOT}/icons)
