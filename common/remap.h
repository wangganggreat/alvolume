#pragma once

#include <cmath>
#include <algorithm>

inline float bias(float f, float b)
{
    if (b > 0.0f) return powf(f, logf(b)/logf(0.5f));
    else return 0.0f;
}

inline float biasandgain(float f, float b, float g)
{
    if (b != 0.5f)
    {
        f = bias(f, b);
    }
    if (g != 0.5f)
    {
        if (f < 0.5f) f = 0.5f * bias(2.0f*f, 1.0f-g);
        else f = 1.0f - bias(2.0f - 2.0f*f, 1.0f-g)*0.5f;
    }
    return f;
}

inline float lerp(const float a, const float b, const float t)
{
   return (1-t)*a + t*b;
}

inline float remap(float f, float in, float ix, bool cn, bool cx, float b, float g, float on, float ox)
{
    
    f = (f - in) / (ix - in);
    
    f = std::max(0.0f, f);
    if (cx) f = std::min(1.0f, f);
    if (b != 0.5f || g != 0.5f) f = biasandgain(f, b, g);
    
    f = lerp(on, ox, f);
    

    return f;
}